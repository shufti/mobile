package youth.bucharest.ro.contentnotification;

import android.app.Application;

/**
 * Created by fslevoaca on 02.06.16.
 */
public class App extends Application {

    public static final String TAG = "App";
    public static final String URL = "http://htmlparser-angrynerds.rhcloud.com:80";
}
