package youth.bucharest.ro.contentnotification.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import youth.bucharest.ro.contentnotification.R;

/**
 * Created by fslevoaca on 31.05.16.
 */
public class RequestListAdapter extends BaseAdapter {

    private Context context;
    private JSONArray list;

    public RequestListAdapter(Context context, JSONArray list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.length();
    }

    @Override
    public Object getItem(int position) {
        try {
            return list.getJSONObject(position);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.request_item_style, parent, false);
        TextView label = (TextView) rowView.findViewById(R.id.labelId);
        TextView url = (TextView) rowView.findViewById(R.id.urlId);
        TextView xpath = (TextView) rowView.findViewById(R.id.xpathId);
        TextView interval = (TextView) rowView.findViewById(R.id.intervalId);
        TextView match = (TextView) rowView.findViewById(R.id.matchId);

        try {
            JSONObject requestJson = list.getJSONObject(position);
            label.setText(requestJson.getString("label"));
            url.setText(requestJson.getString("url"));
            xpath.setText(requestJson.getString("xpath"));
            interval.setText(requestJson.getString("intervalInMinutes"));
            match.setText(requestJson.getString("matchExp"));
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return rowView;
    }
}
