package youth.bucharest.ro.contentnotification.util;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import youth.bucharest.ro.contentnotification.App;

/**
 * Created by raft on 21.05.2016.
 */
@SuppressWarnings("deprecation")
public class AppHttp {
    public static final String TAG = "AppHttp";
    public static final int HTTP_CLIENT_TIMEOUT = 60000;

    public static HttpResponse updateToken(String oldToken, String newToken) throws IOException, JSONException {
        Log.d(TAG, "Send updated token to GCM app server");
        HttpClient httpClient = getClient();
        HttpPut putRequest = new HttpPut(App.URL + "/device/registration");
        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("oldToken", oldToken);
        jsonRequest.put("newToken", newToken);
        putRequest.setEntity(new StringEntity(jsonRequest.toString()));
        putRequest.addHeader("Content-Type", "application/json");
        HttpResponse response = httpClient.execute(putRequest);
        return response;
    }

    public static HttpClient getClient() {
        HttpClient httpClient = new DefaultHttpClient();
        httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, HTTP_CLIENT_TIMEOUT);
        httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, HTTP_CLIENT_TIMEOUT);
        return httpClient;
    }
}
