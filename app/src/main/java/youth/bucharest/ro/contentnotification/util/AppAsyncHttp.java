package youth.bucharest.ro.contentnotification.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import youth.bucharest.ro.contentnotification.App;

/**
 * Created by fslevoaca on 31.05.16.
 */
@SuppressWarnings("deprecation")
public class AppAsyncHttp extends AsyncTask<HttpRequestBase, Void, CustomHttpResponse> {

    public static final String TAG = "AppAsyncHttp";

    protected Context context;

    public AppAsyncHttp(Context context) {
        this.context = context;
    }

    public void getRequests(String token) throws UnsupportedEncodingException {
        Log.i(TAG, "Get requests for device with token " + token);
        HttpPost postRequest = new HttpPost(App.URL + "/parse/request/retrieve");
        postRequest.setHeader("Content-Type", "text/plain");
        if (token != null) {
            postRequest.setEntity(new StringEntity(token));
        }
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, postRequest);
    }

    public void getTemplates() {
        Log.i(TAG, "Get templates");
        HttpGet getRequest = new HttpGet(App.URL + "/parse/template");
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getRequest);
    }

    public void addRequest(JSONObject request) throws UnsupportedEncodingException {
        Log.i(TAG, "Add request" + request.toString());
        HttpPost postRequest = new HttpPost(App.URL + "/parse/request");
        postRequest.setHeader("Content-Type", "application/json");
        postRequest.setEntity(new StringEntity(request.toString()));
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, postRequest);
    }

    public void updateRequest(JSONObject request) throws UnsupportedEncodingException {
        Log.i(TAG, "Update request" + request.toString());
        HttpPut putRequest = new HttpPut(App.URL + "/parse/request");
        putRequest.setHeader("Content-Type", "application/json");
        putRequest.setEntity(new StringEntity(request.toString()));
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, putRequest);
    }

    public void deleteRequest(JSONObject request) throws UnsupportedEncodingException {
        Log.i(TAG, "Delete request" + request.toString());
        HttpPost delRequest = new HttpPost(App.URL + "/parse/request/delete");
        delRequest.setHeader("Content-Type", "application/json");
        delRequest.setEntity(new StringEntity(request.toString()));
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, delRequest);
    }

    public void testRequest(JSONObject request) throws UnsupportedEncodingException {
        Log.i(TAG, "Test request" + request.toString());
        HttpPost testRequest = new HttpPost(App.URL + "/parse/request/test");
        testRequest.setHeader("Content-Type", "application/json");
        testRequest.setEntity(new StringEntity(request.toString()));
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, testRequest);
    }

    @Override
    protected CustomHttpResponse doInBackground(HttpRequestBase... params) {
        try {
            Log.i(TAG, "Making asynchronous http call...");
            HttpClient httpClient = AppHttp.getClient();
            HttpResponse response = httpClient.execute(params[0]);
            int code = response.getStatusLine().getStatusCode();
            return new CustomHttpResponse(code, EntityUtils.toString(response.getEntity()), response.getAllHeaders());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            return new CustomHttpResponse(HttpStatus.SC_BAD_REQUEST, "Cannot connect to server", null);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        }
    }
}
