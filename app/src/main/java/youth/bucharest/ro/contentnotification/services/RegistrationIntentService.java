package youth.bucharest.ro.contentnotification.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.json.JSONException;

import java.io.IOException;

import youth.bucharest.ro.contentnotification.R;
import youth.bucharest.ro.contentnotification.util.AppHttp;
import youth.bucharest.ro.contentnotification.util.QuickStartPreferences;
import youth.bucharest.ro.contentnotification.util.SharedPrefsManager;

/**
 * Created by raft on 19.05.2016.
 */
@SuppressWarnings("deprecation")
public class RegistrationIntentService extends IntentService {

    public static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};

    private SharedPrefsManager sharedPrefsManager;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.sharedPrefsManager = new SharedPrefsManager(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.i(TAG, "Started registering new token...");

        try {
            // [START register_for_gcm]
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
            // See https://developers.google.com/cloud-messaging/android/start for details on this file.
            // [START get_token]
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            // [END get_token]
            Log.i(TAG, "Got gcm registration token: " + token);
            sharedPrefsManager.setToken(token);
            sendRegistrationToServer(token);
            sharedPrefsManager.setTokenRefreshed(true);
            // Subscribe to topic channels
            subscribeTopics(token);
        } catch (Exception e) {
            Log.e(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            sharedPrefsManager.setTokenRefreshed(false);
        } finally {
            notifyCloudSyncReceiver();
        }
    }

    private void sendRegistrationToServer(String token) throws IOException, JSONException {
        String oldToken = sharedPrefsManager.getToken();
        HttpResponse response = AppHttp.updateToken(oldToken, token);
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            throw new RuntimeException(response.getStatusLine().getReasonPhrase());
        }
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }

    private void notifyCloudSyncReceiver() {
        Intent intent = new Intent(QuickStartPreferences.CLOUD_SYNC_ACTION);
        // You can also include some extra data.
        intent.putExtra("message", QuickStartPreferences.UPDATE_STATUS);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
