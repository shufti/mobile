package youth.bucharest.ro.contentnotification.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import youth.bucharest.ro.contentnotification.R;
import youth.bucharest.ro.contentnotification.activities.MainActivity;
import youth.bucharest.ro.contentnotification.util.AppAsyncHttp;
import youth.bucharest.ro.contentnotification.util.CustomHttpResponse;
import youth.bucharest.ro.contentnotification.util.RequestListAdapter;

public class RequestsFragment extends BaseFragment {

    private static final String TAG = "RequestsFragment";
    private ListView requestList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_requests, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.requestList = (ListView) getView().findViewById(R.id.requestListId);
    }

    @Override
    public void onStart() {
        super.onStart();
        requestsLayoutButton.setSelected(true);
        headlinesLayoutButton.setSelected(false);
        try {
            getRequests();
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage(), e);
            showMessageFail("Cannot retrieve list.");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        requestsLayoutButton.setSelected(false);
    }

    private void getRequests() throws UnsupportedEncodingException {
        Log.d(TAG, "Get requests");

        new AppAsyncHttp(getContext()) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessageProgress("Loading...");
            }

            @Override
            @SuppressWarnings("deprecation")
            protected void onPostExecute(CustomHttpResponse response) {
                super.onPostExecute(response);
                if (response == null) {
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                    return;
                }
                if (response.code == HttpStatus.SC_OK) {
                    JSONArray jsonArray = null;
                    try {
                        jsonArray = new JSONArray(response.entity);
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage(), e);
                        showMessageFail(MainActivity.APP_SEVERE_MSG);
                        return;
                    }

                    if (jsonArray.length() != 0) {
                        RequestListAdapter reqListAdapter = new RequestListAdapter(getContext(), jsonArray);
                        requestList.setAdapter(reqListAdapter);
                        requestList.setOnItemClickListener(new RequestListOnItemClickListener(requestList));
                        requestList.setVisibility(View.VISIBLE);
                    } else {
                        requestList.setVisibility(View.GONE);
                    }
                    hideMessage();
                    return;
                } else if (response.code == HttpStatus.SC_BAD_REQUEST) {
                    showMessageFail(response.entity);
                    requestList.setVisibility(View.GONE);
                } else {
                    Log.d(TAG, "Http call failed: " + response.entity);
                    showMessageFail(MainActivity.APP_SERVICE_SEVERE_MSG);
                    requestList.setVisibility(View.GONE);
                }
            }
        }.getRequests(((MainActivity) getActivity()).getSharedPrefsManager().getToken());
    }

    class RequestListOnItemClickListener implements AdapterView.OnItemClickListener {
        private ListView listView;

        public RequestListOnItemClickListener(ListView listView) {
            this.listView = listView;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            JSONObject selectedRequest = (JSONObject) listView.getItemAtPosition(position);
            Log.d(TAG, "Selected item " + selectedRequest);
            loadRequestEditFragment(selectedRequest);
        }
    }

    public void loadRequestEditFragment(JSONObject selectedRequest) {
        FragmentManager fragmentManager = this.getFragmentManager();
        RequestEditFragment requestEditFragment = new RequestEditFragment();
        Bundle args = new Bundle();
        args.putString(MainActivity.REQUEST_EDIT_FRAGMENT_MSG, selectedRequest.toString());
        requestEditFragment.setArguments(args);
        fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_bottom, R.anim.exit_to_top)
                .replace(R.id.fragmentContainerId, requestEditFragment, MainActivity.REQUEST_EDIT_FRAGMENT_TAG).commit();
    }
}
