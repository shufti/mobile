package youth.bucharest.ro.contentnotification.util;

import org.apache.http.Header;

/**
 * Created by fslevoaca on 31.05.16.
 */
@SuppressWarnings("deprecation")
public class CustomHttpResponse {

    public int code;
    public String entity;
    public Header[] headers;

    public CustomHttpResponse(int code, String entity, Header[] headers) {
        this.code = code;
        this.entity = entity;
        this.headers = headers;
    }

    public String getHeaderValue(String name) {
        for (Header header : headers) {
            if (header.getName().equals(name)) {
                return header.getValue();
            }
        }
        return null;
    }
}