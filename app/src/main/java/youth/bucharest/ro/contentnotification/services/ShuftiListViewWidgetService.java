package youth.bucharest.ro.contentnotification.services;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import youth.bucharest.ro.contentnotification.R;
import youth.bucharest.ro.contentnotification.util.SharedPrefsGcmResultsManager;
import youth.bucharest.ro.contentnotification.util.UpdateListAdapter;

/**
 * Created by fslevoaca on 10.06.16.
 */
public class ShuftiListViewWidgetService extends RemoteViewsService {

    static final String TAG = "ShuftiRemoteViewService";

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new ShuftiListViewRemoteFactory(getApplicationContext(), intent);
    }
}

class ShuftiListViewRemoteFactory implements RemoteViewsService.RemoteViewsFactory {

    private Context mContext;
    private List<JSONObject> results;
    private SharedPrefsGcmResultsManager sharedPrefsManager;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM");
    private SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");


    public ShuftiListViewRemoteFactory(Context context, Intent intent) {
        this.mContext = context;
    }

    @Override
    //Avoid expensive operations here
    public void onCreate() {
        Log.d(ShuftiListViewWidgetService.TAG, "Creating remoteView service");
        this.sharedPrefsManager = new SharedPrefsGcmResultsManager(mContext);
        this.results = new ArrayList<>();
        Log.d(ShuftiListViewWidgetService.TAG, "RemoteView service created");
    }

    @Override
    public void onDataSetChanged() {
        try {
            this.results = getPersistedResults();
            Log.d(ShuftiListViewWidgetService.TAG, "GOT " + this.results.size() + " non empty results");
        } catch (JSONException e) {
            Log.e(ShuftiListViewWidgetService.TAG, e.getMessage(), e);
        }
        Log.d(ShuftiListViewWidgetService.TAG, "RemoteView dataset updated");
    }

    @Override
    public void onDestroy() {
        //TO DO ?
    }

    @Override
    public int getCount() {
        return this.results.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {

        RemoteViews mView = new RemoteViews(mContext.getPackageName(),
                R.layout.widget_result_item);

        //Update ListView
        try {
            Log.d(ShuftiListViewWidgetService.TAG, "Draw data from JSON " + results.get(position));
            String label = results.get(position).getString("label");
            String content = results.get(position).getJSONObject("result").getString("content");
            Long ts = results.get(position).getJSONObject("result").getLong("procEndTime");

            String date = null;
            String time = null;

            if (ts != null) {
                date = dateFormat.format(ts);
                time = timeFormat.format(ts);
            }

            mView.setTextViewText(R.id.labelId, (label != null && !label.isEmpty()) ? label : "No label");
            mView.setTextViewText(R.id.contentId, (content != null && !content.isEmpty()) ? content : "No content found");
            mView.setTextViewText(R.id.dateId, (date != null) ? date : "No date");
            mView.setTextViewText(R.id.timeId, (time != null) ? time : "No time");

            mView.setOnClickFillInIntent(R.id.widgetItemId, UpdateListAdapter.getOpenInBrowserIntent(results.get(position)));

        } catch (JSONException e) {
            Log.e(ShuftiListViewWidgetService.TAG, e.getMessage(), e);
        }
        Log.d(ShuftiListViewWidgetService.TAG, "RETURN VIEW AT POSITION " + position);
        return mView;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    private List<JSONObject> getPersistedResults() throws JSONException {
        List<JSONObject> results = new ArrayList<>();
        Map<String, String> rawResults = (Map<String, String>) sharedPrefsManager.getAllResults();
        for (Map.Entry<String, String> rawResult : rawResults.entrySet()) {
            results.add(new JSONObject(rawResult.getValue()));
        }
        return results;
    }
}
