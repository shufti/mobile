package youth.bucharest.ro.contentnotification.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import youth.bucharest.ro.contentnotification.R;
import youth.bucharest.ro.contentnotification.services.ShuftiListViewWidgetService;
import youth.bucharest.ro.contentnotification.util.SharedPrefsGcmResultsManager;

public class ResultsWidgetProvider extends AppWidgetProvider {

    public static final String TAG = "ResultsWidgetProvider";
    public static final String ACTION = "youth.bucharest.ro.contentnotification.widgets.WIDGET_RESULTS_UPDATE";

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.d(TAG, "Enabling Shufti widget...");
        Log.d(TAG, "Widget enabled");
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction().equals(ACTION)) {
            Log.d(TAG, "Received intent for updating Shufti widgets");
            updateWidgets(context);
        }
        Log.d(TAG, "Shufti widgets updated");
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        Log.d(TAG, "ON_UPDATE widget provider method called...");
        updateWidgets(context);
        Log.d(TAG, "FINISHED WIDGETS UPDATE");
    }

    private void updateWidgets(Context context) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        ComponentName thisWidgetProvider = new ComponentName(context,
                ResultsWidgetProvider.class);

        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidgetProvider);

        Log.d(TAG, "Update views for " + allWidgetIds.length + " widgets");
        appWidgetManager.notifyAppWidgetViewDataChanged(allWidgetIds, R.id.widgetListViewId);
        Log.d(TAG, "Invalidated data for " + allWidgetIds.length + " views");

        RemoteViews remoteViews = initViews(context);
        appWidgetManager.updateAppWidget(allWidgetIds, remoteViews);
    }

    private RemoteViews initViews(Context context) {
        RemoteViews mView = new RemoteViews(context.getPackageName(),
                R.layout.widget_layout);
        SharedPrefsGcmResultsManager sharedPrefsManager = new SharedPrefsGcmResultsManager(context);
        if (sharedPrefsManager.getResultsListSize() == 0) {
            mView.setViewVisibility(R.id.widgetListViewId, View.GONE);
            mView.setViewVisibility(R.id.widgetEmptyViewId, View.VISIBLE);
        } else {
            mView.setViewVisibility(R.id.widgetListViewId, View.VISIBLE);
            mView.setViewVisibility(R.id.widgetEmptyViewId, View.GONE);
            Intent intent = new Intent(context, ShuftiListViewWidgetService.class);
            mView.setRemoteAdapter(R.id.widgetListViewId, intent);
            mView.setPendingIntentTemplate(R.id.widgetListViewId, getActivityPendingIntentTemplate(context));
        }
        return mView;
    }

    private PendingIntent getActivityPendingIntentTemplate(Context context) {
        Log.d(TAG, "Set empty pending intent template");
        Intent intent = new Intent();
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
        return pIntent;
    }
}