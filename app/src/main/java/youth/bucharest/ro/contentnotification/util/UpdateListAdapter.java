package youth.bucharest.ro.contentnotification.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import youth.bucharest.ro.contentnotification.R;

/**
 * Created by fslevoaca on 31.05.16.
 */
public class UpdateListAdapter extends BaseAdapter {

    public static final String TAG = "UpdateListAdapter";

    private Context context;
    private JSONArray list;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM");
    private SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");


    public UpdateListAdapter(Context context, JSONArray list) throws JSONException {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.length();
    }

    @Override
    public Object getItem(int position) {
        try {
            return list.getJSONObject(position);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.widget_result_item, parent, false);
        TextView labelView = (TextView) rowView.findViewById(R.id.labelId);
        TextView dateView = (TextView) rowView.findViewById(R.id.dateId);
        TextView timeView = (TextView) rowView.findViewById(R.id.timeId);
        TextView contentView = (TextView) rowView.findViewById(R.id.contentId);

        try {
            JSONObject updateJson = list.getJSONObject(position);
            labelView.setText(updateJson.getString("label"));

            String content = updateJson.getJSONObject("result").getString("content");
            contentView.setText(content != null && !content.isEmpty() ? content : "No content");

            Long ts = updateJson.getJSONObject("result").getLong("procEndTime");
            String date = null;
            String time = null;

            if (ts != null) {
                date = dateFormat.format(ts);
                time = timeFormat.format(ts);
            }

            dateView.setText(date != null ? date : "No date");
            timeView.setText(time != null ? time : "No time");
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return rowView;
    }

    public static List<JSONObject> extractItemsWithResults(JSONArray updateList) throws JSONException {
        List<JSONObject> updateWithResultList = new ArrayList<>();
        for (int i = 0; i < updateList.length(); i++) {
            if (!updateList.getJSONObject(i).isNull("result")) {
                updateWithResultList.add(updateList.getJSONObject(i));
            }
        }
        return updateWithResultList;
    }

    public static Intent getOpenInBrowserIntent(JSONObject result) throws JSONException {
        return new Intent(Intent.ACTION_VIEW, Uri.parse(result.getString("url")));
    }
}
