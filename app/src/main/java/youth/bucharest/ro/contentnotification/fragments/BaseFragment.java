package youth.bucharest.ro.contentnotification.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import youth.bucharest.ro.contentnotification.R;
import youth.bucharest.ro.contentnotification.util.SharedPrefsGcmResultsManager;
import youth.bucharest.ro.contentnotification.widgets.ResultsWidgetProvider;

/**
 * Created by fslevoaca on 31.05.16.
 */
public class BaseFragment extends Fragment {

    private static final String TAG = "BaseFragment";

    private View messageView;
    private TextView messageText;
    private ProgressBar pb;

    protected LinearLayout headlinesLayoutButton;
    protected LinearLayout requestsLayoutButton;

    protected SharedPrefsGcmResultsManager sharedPrefsResultsManager;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.headlinesLayoutButton = (LinearLayout) getActivity().findViewById(R.id.headlinesLayoutButtonId);
        this.requestsLayoutButton = (LinearLayout) getActivity().findViewById(R.id.requestsLayoutButtonId);

        this.messageView = view.findViewById(R.id.messageLayoutId);
        this.messageText = (TextView) view.findViewById(R.id.messageTextId);
        this.pb = (ProgressBar) view.findViewById(R.id.pbProgress);

        this.messageView.setVisibility(View.INVISIBLE);
        this.pb.setVisibility(View.GONE);
        hideKeyboard();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.sharedPrefsResultsManager = new SharedPrefsGcmResultsManager(context);
    }

    protected void showMessageSuccess(String msg) {
        this.pb.setVisibility(View.GONE);
        this.messageView.setVisibility(View.VISIBLE);
        this.messageText.setTextColor(Color.parseColor("#44646c"));
        this.messageText.setText(msg);
    }

    protected void showMessageFail(String msg) {
        this.pb.setVisibility(View.GONE);
        this.messageView.setVisibility(View.VISIBLE);
        this.messageText.setText(msg);
        this.messageText.setTextColor(Color.parseColor("#FFC22F27"));
    }

    protected void showMessageProgress(String msg) {
        this.messageView.setVisibility(View.VISIBLE);
        this.messageText.setText(msg);
        this.messageText.setTextColor(Color.parseColor("#44646c"));
        this.pb.setVisibility(View.VISIBLE);
    }

    protected void hideMessage() {
        this.messageView.setVisibility(View.INVISIBLE);
    }

    protected void hideKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    protected void notifyWidgetProviderToUpdateViews(Context context) {
        Intent intent = new Intent(ResultsWidgetProvider.ACTION);
        context.sendBroadcast(intent);
        Log.d(TAG, "Sent intent to update all widgets");
    }
}
