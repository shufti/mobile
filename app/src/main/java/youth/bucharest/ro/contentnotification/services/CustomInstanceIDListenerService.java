package youth.bucharest.ro.contentnotification.services;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by raft on 19.05.2016.
 */
public class CustomInstanceIDListenerService extends InstanceIDListenerService {
    private static final String TAG = "InstanceIDLS";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    @Override
    public void onTokenRefresh() {
        Log.i(TAG, "Need to refresh token ! Proceed to new token registration...");
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
        stopSelf();
    }
    // [END refresh_token]
}
