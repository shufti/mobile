package youth.bucharest.ro.contentnotification.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import youth.bucharest.ro.contentnotification.R;
import youth.bucharest.ro.contentnotification.activities.MainActivity;
import youth.bucharest.ro.contentnotification.util.AppAsyncHttp;
import youth.bucharest.ro.contentnotification.util.CustomHttpResponse;

/**
 * Created by fslevoaca on 06.06.16.
 */
public class RequestConfigFragment extends BaseFragment {

    private static final String TAG = "RequestConfigFragment";

    protected TextView labelText;
    protected TextView urlText;
    protected TextView xpathText;
    protected TextView matchText;
    protected TextView intervalText;

    private AppAsyncHttp httpAsyncTask;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.labelText = (TextView) getActivity().findViewById(R.id.labelId);
        this.urlText = (TextView) getActivity().findViewById(R.id.urlId);
        this.xpathText = (TextView) getActivity().findViewById(R.id.xpathId);
        this.matchText = (TextView) getActivity().findViewById(R.id.matchExpId);
        this.intervalText = (TextView) getActivity().findViewById(R.id.intervalId);

        setXpathHelpButtonOnClickListener();
        setMatchHelpButtonOnClickListener();
        setTestRequestButtonOnClickListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        this.requestsLayoutButton.setSelected(false);
        this.headlinesLayoutButton.setSelected(false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (httpAsyncTask != null) {
            httpAsyncTask.cancel(true);
        }
    }

    private JSONObject getJSONRequestFromCurrentView() throws JSONException {
        JSONObject request = new JSONObject();
        request.put("label", this.labelText.getText());
        request.put("url", this.urlText.getText());
        request.put("xpath", this.xpathText.getText());
        request.put("matchExp", this.matchText.getText());
        request.put("intervalInMinutes", Integer.parseInt(this.intervalText.getText().toString()) * 60);
        request.put("token", ((MainActivity) getActivity()).getSharedPrefsManager().getToken());
        return request;
    }

    private void setXpathHelpButtonOnClickListener() {
        View xpathHelpButton = getActivity().findViewById(R.id.xpathHelpId);
        xpathHelpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.xpath_help_dialog, null);
                builder.setView(dialogView).setPositiveButton(android.R.string.ok, null).create().show();
            }
        });
    }

    private void setMatchHelpButtonOnClickListener() {
        View xpathHelpButton = getActivity().findViewById(R.id.matchHelpId);
        xpathHelpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.match_help_dialog, null);
                builder.setView(dialogView).setPositiveButton(android.R.string.ok, null).create().show();
            }
        });
    }

    private void setTestRequestButtonOnClickListener() {
        View previewBtn = getActivity().findViewById(R.id.previewBtn);
        previewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    testRequest(getJSONRequestFromCurrentView());
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                }
            }
        });
    }

    private void showTestResult(JSONObject result) throws JSONException {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.test_result_dialog, null);
        builder.setView(dialogView);

        TextView labelTestResultText = (TextView) dialogView.findViewById(R.id.labelTestId);
        TextView xpathTestResultText = (TextView) dialogView.findViewById(R.id.xpathResultTestId);
        TextView matchTestResultText = (TextView) dialogView.findViewById(R.id.matchTestId);
        ImageView resultImage = (ImageView) dialogView.findViewById(R.id.xpathResultImgId);
        ImageView matchExpImage = (ImageView) dialogView.findViewById(R.id.equalsMatchExpId);

        String xpathResult = result.getJSONObject("result").getString("content");
        boolean isMatch = result.getJSONObject("result").getBoolean("match");

        labelTestResultText.setText(result.getString("label"));
        if (xpathResult != null && !xpathResult.isEmpty()) {
            xpathTestResultText.setText(xpathResult);
            resultImage.setImageResource(R.drawable.check);
        } else {
            xpathTestResultText.setText("No content found");
            resultImage.setImageResource(R.drawable.exclamation);
        }
        if (isMatch) {
            matchExpImage.setImageResource(R.drawable.check);
            matchTestResultText.setText("Content is matched by your regular expression");
        } else {
            matchExpImage.setImageResource(R.drawable.exclamation_gray);
            matchTestResultText.setText("Content is NOT matched by your regular expression");
        }
        builder.setPositiveButton(android.R.string.ok, null).create().show();
    }

    private void testRequest(JSONObject request) throws UnsupportedEncodingException {

        Log.d(TAG, "Test request");
        httpAsyncTask = new AppAsyncHttp(getContext()) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                hideKeyboard();
                showMessageProgress("Processing...");
            }

            @Override
            @SuppressWarnings("deprecation")
            protected void onPostExecute(CustomHttpResponse response) {
                super.onPostExecute(response);
                if (response == null) {
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                    return;
                }
                if (response.code == HttpStatus.SC_OK) {
                    try {
                        showTestResult(new JSONObject(response.entity));
                        hideMessage();
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage(), e);
                        showMessageFail(MainActivity.APP_SEVERE_MSG);
                    }
                } else if (response.code == HttpStatus.SC_BAD_REQUEST) {
                    Log.d(TAG, "Http call failed: " + response.entity);
                    showMessageFail(response.entity);
                } else {
                    Log.d(TAG, "Http call failed: " + response.entity);
                    showMessageFail(MainActivity.APP_SERVICE_SEVERE_MSG);
                }
            }
        };
        httpAsyncTask.testRequest(request);
    }
}