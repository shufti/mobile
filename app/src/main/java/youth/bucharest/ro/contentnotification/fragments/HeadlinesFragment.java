package youth.bucharest.ro.contentnotification.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import youth.bucharest.ro.contentnotification.R;
import youth.bucharest.ro.contentnotification.activities.MainActivity;
import youth.bucharest.ro.contentnotification.util.AppAsyncHttp;
import youth.bucharest.ro.contentnotification.util.CustomHttpResponse;
import youth.bucharest.ro.contentnotification.util.UpdateListAdapter;

public class HeadlinesFragment extends BaseFragment {

    private static final String TAG = "HeadlinesFragment";

    private ListView updateListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_headlines, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.updateListView = (ListView) getView().findViewById(R.id.updateListId);
        setSyncUpdatesButtonOnClickListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        requestsLayoutButton.setSelected(false);
        headlinesLayoutButton.setSelected(true);
        loadUpdates();
    }

    @Override
    public void onStop() {
        super.onStop();
        headlinesLayoutButton.setSelected(false);
    }

    private void setSyncUpdatesButtonOnClickListener() {
        View syncUpdatesButton = getActivity().findViewById(R.id.syncUpdatesBtn);
        syncUpdatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getRequests();
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                }
            }
        });
    }

    private void loadUpdates() {

        Log.d(TAG, "Load list of updates");
        new AsyncTask<Void, Void, JSONArray>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(JSONArray updateList) {
                super.onPostExecute(updateList);
                if (updateList == null) {
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                    return;
                }

                if (updateList.length() == 0) {
                    HeadlinesFragment.this.updateListView.setVisibility(View.GONE);
                    return;
                }
                UpdateListAdapter updateListAdapter = null;
                try {
                    HeadlinesFragment.this.updateListView.setVisibility(View.VISIBLE);
                    updateListAdapter = new UpdateListAdapter(getContext(), updateList);
                    HeadlinesFragment.this.updateListView.setAdapter(updateListAdapter);
                    HeadlinesFragment.this.updateListView.setOnItemClickListener(new UpdateListOnItemClickListener(HeadlinesFragment.this.updateListView));
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage(), e);
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                }
            }

            @Override
            protected JSONArray doInBackground(Void... params) {
                JSONArray updateList = new JSONArray();
                Map<String, ?> persistedUpdates = HeadlinesFragment.this.sharedPrefsResultsManager.getAllResults();
                for (Map.Entry<String, ?> entry : persistedUpdates.entrySet()) {
                    try {
                        updateList.put(new JSONObject((String) entry.getValue()));
                    } catch (JSONException e) {
                        return null;
                    }
                }
                return updateList;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void getRequests() throws UnsupportedEncodingException {

        Log.d(TAG, "Load requests");
        new AppAsyncHttp(getContext()) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessageProgress("Loading...");
            }

            @Override
            @SuppressWarnings("deprecation")
            protected void onPostExecute(CustomHttpResponse response) {
                super.onPostExecute(response);
                if (response == null) {
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                    return;
                }
                if (response.code == HttpStatus.SC_OK) {
                    List<JSONObject> updateList = null;
                    try {
                        updateList = UpdateListAdapter.extractItemsWithResults(new JSONArray(response.entity));
                        Log.d(TAG, "Got " + updateList.size() + " updates");
                        sharedPrefsResultsManager.syncResults(updateList);
                        notifyWidgetProviderToUpdateViews(this.context);
                        loadUpdates();
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage(), e);
                        showMessageFail(MainActivity.APP_SEVERE_MSG);
                        return;
                    }
                    hideMessage();
                    return;
                } else if (response.code == HttpStatus.SC_BAD_REQUEST) {
                    showMessageFail(response.entity);
                } else {
                    Log.d(TAG, "Http call failed: " + response.entity);
                    showMessageFail(MainActivity.APP_SERVICE_SEVERE_MSG);
                }
            }
        }.getRequests(((MainActivity) getActivity()).getSharedPrefsManager().getToken());
    }

    //On item click, open in browser
    class UpdateListOnItemClickListener implements AdapterView.OnItemClickListener {
        private ListView listView;

        public UpdateListOnItemClickListener(ListView listView) {
            this.listView = listView;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            JSONObject selectedPersistedUpdate = (JSONObject) listView.getItemAtPosition(position);
            Log.d(TAG, "Selected item " + selectedPersistedUpdate);
            try {
                openInBrowser(selectedPersistedUpdate.getString("url"));
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage(), e);
                showMessageFail(MainActivity.APP_SEVERE_MSG);
            }
        }
    }

    public void openInBrowser(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}
