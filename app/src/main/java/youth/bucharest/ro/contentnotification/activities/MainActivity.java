package youth.bucharest.ro.contentnotification.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import youth.bucharest.ro.contentnotification.R;
import youth.bucharest.ro.contentnotification.fragments.HeadlinesFragment;
import youth.bucharest.ro.contentnotification.fragments.RequestAddFragment;
import youth.bucharest.ro.contentnotification.fragments.RequestsFragment;
import youth.bucharest.ro.contentnotification.services.RegistrationIntentService;
import youth.bucharest.ro.contentnotification.util.QuickStartPreferences;
import youth.bucharest.ro.contentnotification.util.SharedPrefsManager;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static final String APP_SEVERE_MSG = "Something bad has happened";
    public static final String APP_SERVICE_SEVERE_MSG = "Failed service";

    public static final String REQUESTS_FRAGMENT_TAG = "REQUESTS_FRAGMENT_TAG";
    public static final String HEADLINES_FRAGMENT_TAG = "HEADLINES_FRAGMENT_TAG";
    public static final String REQUEST_EDIT_FRAGMENT_TAG = "REQUEST_EDIT_FRAGMENT_TAG";
    public static final String REQUEST_ADD_FRAGMENT_TAG = "REQUEST_ADD_FRAGMENT_TAG";
    public static final String REQUEST_EDIT_FRAGMENT_MSG = "REQUEST_EDIT_FRAGMENT_MSG";

    private CloudSyncListener cloudSyncReceiver;
    private NetworkChangeReceiver networkChangeReceiver;

    protected SharedPrefsManager sharedPrefsManager;

    private boolean isCloudSyncReceiverRegistered;
    private boolean isNetworkChangeReceiverRegistered;

    private TextView syncStatusView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.syncStatusView = (TextView) findViewById(R.id.syncStatusId);

        this.cloudSyncReceiver = new CloudSyncListener();
        this.networkChangeReceiver = new NetworkChangeReceiver();
        registerCloudSyncReceiver(this.cloudSyncReceiver);
        registerNetworkChangeReceiver(this.networkChangeReceiver);

        this.sharedPrefsManager = new SharedPrefsManager(getApplicationContext());

        if (checkPlayServices()) {
            Log.d(TAG, "Start IntentService to register this application with GCM");
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        loadHeadlinesFragment(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterCloudSyncReceiver(cloudSyncReceiver);
        unregisterNetworkChangeReceiver(networkChangeReceiver);
    }

    public SharedPrefsManager getSharedPrefsManager() {
        return sharedPrefsManager;
    }

    // User action
    public void refreshToken(View view) {
        // Start IntentService to register this application with GCM.
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
        this.syncStatusView.setText("Sync...");
        this.syncStatusView.setTextColor(Color.LTGRAY);
    }

    // User action
    public void loadHeadlinesFragment(View view) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        HeadlinesFragment headlinesFragment = (HeadlinesFragment) fragmentManager.findFragmentByTag(HEADLINES_FRAGMENT_TAG);
        if (headlinesFragment == null || !headlinesFragment.isVisible()) {
            headlinesFragment = new HeadlinesFragment();
            getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right)
                    .replace(R.id.fragmentContainerId, headlinesFragment, HEADLINES_FRAGMENT_TAG).commit();
        }
    }

    // User action
    public void loadRequestsFragment(View view) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        RequestsFragment requestsFragment = (RequestsFragment) fragmentManager.findFragmentByTag(REQUESTS_FRAGMENT_TAG);
        if (requestsFragment == null || !requestsFragment.isVisible()) {
            requestsFragment = new RequestsFragment();
            getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                    .replace(R.id.fragmentContainerId, requestsFragment, REQUESTS_FRAGMENT_TAG).commit();
        }
    }

    // User action
    public void loadRequestAddFragment(View view) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        RequestAddFragment requestAddFragment = (RequestAddFragment) fragmentManager.findFragmentByTag(REQUEST_ADD_FRAGMENT_TAG);
        if (requestAddFragment == null || !requestAddFragment.isVisible()) {
            requestAddFragment = new RequestAddFragment();
            getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_bottom, R.anim.exit_to_top)
                    .replace(R.id.fragmentContainerId, requestAddFragment, REQUEST_ADD_FRAGMENT_TAG).commit();
        }
    }

    // User action
    public void help(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.help_dialog, null);
        builder.setView(dialogView).setPositiveButton(android.R.string.ok, null).create().show();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        HeadlinesFragment headlinesFragment = (HeadlinesFragment) fragmentManager.findFragmentByTag(HEADLINES_FRAGMENT_TAG);
        if (headlinesFragment == null || !headlinesFragment.isVisible()) {
            loadHeadlinesFragment(null);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void checkTokenIsSentToServer() {
        Log.d(TAG, "Check token is sent to server");
        String token = sharedPrefsManager.getToken();
        boolean isSentToServer = sharedPrefsManager.isTokenRefreshed();
        if (token != null && !isSentToServer) {
            Log.i(TAG, "Restart service " + RegistrationIntentService.TAG);
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private void unregisterCloudSyncReceiver(BroadcastReceiver receiver) {
        if (isCloudSyncReceiverRegistered) {
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(receiver);
            isCloudSyncReceiverRegistered = false;
        }
    }

    private void unregisterNetworkChangeReceiver(BroadcastReceiver receiver) {
        if (isNetworkChangeReceiverRegistered) {
            unregisterReceiver(receiver);
            isNetworkChangeReceiverRegistered = false;
        }
    }

    private void registerCloudSyncReceiver(BroadcastReceiver receiver) {
        if (!isCloudSyncReceiverRegistered) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(QuickStartPreferences.CLOUD_SYNC_ACTION);
            LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(receiver, intentFilter);
            isCloudSyncReceiverRegistered = true;
        }
    }

    private void registerNetworkChangeReceiver(BroadcastReceiver receiver) {
        if (!isNetworkChangeReceiverRegistered) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(receiver, intentFilter);
            isNetworkChangeReceiverRegistered = true;
        }
    }

    class CloudSyncListener extends BroadcastReceiver {

        public static final String TAG = "CloudSyncListener";

        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d(TAG, "Got message: " + message);
            if (message != null && message.equals(QuickStartPreferences.UPDATE_STATUS)) {
                if (!sharedPrefsManager.isTokenRefreshed()) {
                    syncStatusView.setText("Ups");
                    syncStatusView.setTextColor(Color.parseColor("#FA3E3E"));
                } else {
                    syncStatusView.setText("OK");
                    syncStatusView.setTextColor(Color.parseColor("#E1E8CF"));
                }
            }
        }
    }

    class NetworkChangeReceiver extends BroadcastReceiver {

        public static final String TAG = "NetworkChangeReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "NETWORK STATUS CHANGED");
            Intent registrationIntent = new Intent(context, RegistrationIntentService.class);
            startService(registrationIntent);
        }
    }
}
