package youth.bucharest.ro.contentnotification.fragments;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import youth.bucharest.ro.contentnotification.R;
import youth.bucharest.ro.contentnotification.activities.MainActivity;
import youth.bucharest.ro.contentnotification.util.AppAsyncHttp;
import youth.bucharest.ro.contentnotification.util.CustomHttpResponse;
import youth.bucharest.ro.contentnotification.util.TemplateListAdapter;

/**
 * Created by fslevoaca on 02.06.16.
 */
public class RequestAddFragment extends RequestConfigFragment {

    private static final String TAG = "RequestAddFragment";

    private JSONObject selectedTemplate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request_add, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setAddButtonOnClickListener();
        setTemplateButtonOnClickListener();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void setAddButtonOnClickListener() {
        View addLayoutButton = getActivity().findViewById(R.id.addBtn);
        addLayoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addRequest(getJSONRequestFromCurrentView());
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                }
            }
        });
    }

    private void setTemplateButtonOnClickListener() {
        View templateBtn = getActivity().findViewById(R.id.templateBtn);
        templateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getTemplates();
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                }
            }
        });
    }

    private JSONObject getJSONRequestFromCurrentView() throws JSONException {
        JSONObject request = new JSONObject();
        request.put("label", this.labelText.getText());
        request.put("url", this.urlText.getText());
        request.put("xpath", this.xpathText.getText());
        request.put("matchExp", this.matchText.getText());
        request.put("intervalInMinutes", /*Integer.parseInt(*/this.intervalText.getText()/*.toString()) * 60*/);
        request.put("token", ((MainActivity) getActivity()).getSharedPrefsManager().getToken());
        return request;
    }

    private void addRequest(JSONObject request) throws UnsupportedEncodingException {

        Log.d(TAG, "Add request");
        new AppAsyncHttp(getContext()) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                hideKeyboard();
                showMessageProgress("Loading...");
            }

            @Override
            @SuppressWarnings("deprecation")
            protected void onPostExecute(CustomHttpResponse response) {
                super.onPostExecute(response);
                if (response == null) {
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                    return;
                }
                if (response.code == HttpStatus.SC_OK) {
                    showMessageSuccess("SUCCESS");
                } else if (response.code == HttpStatus.SC_BAD_REQUEST) {
                    Log.d(TAG, "Http call failed: " + response.entity);
                    showMessageFail(response.entity);
                } else {
                    Log.d(TAG, "Http call failed: " + response.entity);
                    showMessageFail(MainActivity.APP_SERVICE_SEVERE_MSG);
                }
            }
        }.addRequest(request);
    }

    private void getTemplates() {
        Log.d(TAG, "Get templates");

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.templates_dialog, null);
        builder.setView(dialogView);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    if (applySelectedTemplate()) {
                        showMessageSuccess("Template successfully applied");
                    } else {
                        showMessageSuccess("No template selected");
                    }
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage(), e);
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                }
            }
        })
                .setNegativeButton(android.R.string.cancel, null).create();
        builder.show();

        final ListView templateList = (ListView) dialogView.findViewById(R.id.templateListId);
        final View emptyTemplateListView = dialogView.findViewById(R.id.emptyTemplateViewId);

        final View dialogMessageView = dialogView.findViewById(R.id.dialogMessageViewId);
        final View dialogProgressBar = dialogView.findViewById(R.id.dialogProgressId);
        final TextView dialogMessageText = (TextView) dialogView.findViewById(R.id.dialogMessageTextId);

        new AppAsyncHttp(getContext()) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialogMessageView.setVisibility(View.VISIBLE);
                dialogProgressBar.setVisibility(View.VISIBLE);
                dialogMessageText.setVisibility(View.VISIBLE);
                templateList.setVisibility(View.GONE);
                emptyTemplateListView.setVisibility(View.GONE);
                dialogMessageText.setText("Loading templates...");
            }

            @Override
            @SuppressWarnings("deprecation")
            protected void onPostExecute(CustomHttpResponse response) {
                super.onPostExecute(response);
                if (response == null) {
                    dialogMessageView.setVisibility(View.VISIBLE);
                    dialogProgressBar.setVisibility(View.GONE);
                    dialogMessageText.setVisibility(View.VISIBLE);
                    dialogMessageText.setText(MainActivity.APP_SEVERE_MSG);
                    return;
                }
                if (response.code == HttpStatus.SC_OK) {

                    dialogMessageView.setVisibility(View.GONE);
                    try {
                        final JSONArray jsonArray = new JSONArray(response.entity);

                        if (jsonArray.length() != 0) {
                            final TemplateListAdapter templateListAdapter = new TemplateListAdapter(getContext(), jsonArray);
                            templateList.setAdapter(templateListAdapter);
                            templateList.setVisibility(View.VISIBLE);
                            emptyTemplateListView.setVisibility(View.GONE);

                            templateList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                                    selectedTemplate = (JSONObject) templateList.getAdapter().getItem(position);
                                    Log.d(TAG, "Selected template is: " + selectedTemplate);
                                }
                            });
                        } else {
                            templateList.setVisibility(View.GONE);
                            emptyTemplateListView.setVisibility(View.VISIBLE);
                        }
                        hideMessage();
                        return;
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage(), e);
                        dialogMessageView.setVisibility(View.VISIBLE);
                        dialogProgressBar.setVisibility(View.GONE);
                        dialogMessageText.setVisibility(View.VISIBLE);
                        dialogMessageText.setText(MainActivity.APP_SEVERE_MSG);
                        return;
                    }
                } else if (response.code == HttpStatus.SC_BAD_REQUEST) {
                    dialogMessageView.setVisibility(View.VISIBLE);
                    dialogProgressBar.setVisibility(View.GONE);
                    dialogMessageText.setVisibility(View.VISIBLE);
                    dialogMessageText.setText(response.entity);
                } else {
                    Log.d(TAG, "Http call failed: " + response.entity);
                    dialogMessageView.setVisibility(View.VISIBLE);
                    dialogProgressBar.setVisibility(View.GONE);
                    dialogMessageText.setVisibility(View.VISIBLE);
                    dialogMessageText.setText(MainActivity.APP_SEVERE_MSG);
                }
            }
        }.getTemplates();
    }

    private boolean applySelectedTemplate() throws JSONException {
        if (this.selectedTemplate != null) {
            this.labelText.setText(selectedTemplate.getString("label"));
            this.urlText.setText(selectedTemplate.getString("url"));
            this.xpathText.setText(selectedTemplate.getString("xpath"));
            return true;
        }
        return false;
    }
}
