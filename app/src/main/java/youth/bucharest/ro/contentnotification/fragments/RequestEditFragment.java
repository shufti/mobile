package youth.bucharest.ro.contentnotification.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import youth.bucharest.ro.contentnotification.R;
import youth.bucharest.ro.contentnotification.activities.MainActivity;
import youth.bucharest.ro.contentnotification.util.AppAsyncHttp;
import youth.bucharest.ro.contentnotification.util.CustomHttpResponse;

/**
 * Created by fslevoaca on 02.06.16.
 */
public class RequestEditFragment extends RequestConfigFragment {

    private static final String TAG = "RequestEditFragment";

    private JSONObject selectedRequest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request_edit, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setEditButtonOnClickListener();
        setDeleteButtonOnClickListener();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        try {
            this.selectedRequest = getSelectedRequestFromArgs();
            Log.d(TAG, "Got request " + selectedRequest);
            fillCurrentViewFromSelectedRequest(this.selectedRequest);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
            showMessageFail(MainActivity.APP_SEVERE_MSG);
        }
    }

    private void setEditButtonOnClickListener() {
        View addLayoutButton = getActivity().findViewById(R.id.addBtn);
        addLayoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    updateRequest(getJSONRequestFromCurrentView());
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                }
            }
        });
    }

    private void setDeleteButtonOnClickListener() {
        View delLayoutButton = getActivity().findViewById(R.id.deleteBtn);
        delLayoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRequestWithConfirmation();
            }
        });
    }

    private JSONObject getSelectedRequestFromArgs() throws JSONException {
        return new JSONObject(getArguments().getString(MainActivity.REQUEST_EDIT_FRAGMENT_MSG));
    }

    private void fillCurrentViewFromSelectedRequest(JSONObject request) throws JSONException {
        this.labelText.setText(request.getString("label"));
        this.urlText.setText(request.getString("url"));
        this.xpathText.setText(request.getString("xpath"));
        this.matchText.setText(request.getString("matchExp"));
        this.intervalText.setText(request.getString("intervalInMinutes"));
    }

    private JSONObject getJSONRequestFromCurrentView() throws JSONException {
        JSONObject request = new JSONObject();
        request.put("label", this.labelText.getText());
        request.put("url", this.urlText.getText());
        request.put("xpath", this.xpathText.getText());
        request.put("matchExp", this.matchText.getText());
        request.put("intervalInMinutes", /*Integer.parseInt(*/this.intervalText.getText()/*.toString()) * 60*/);
        request.put("token", ((MainActivity) getActivity()).getSharedPrefsManager().getToken());
        request.put("id", this.selectedRequest.getInt("id"));
        return request;
    }


    public void deleteRequestWithConfirmation() {
        new AlertDialog.Builder(this.getContext())
                .setTitle("Confirmation")
                .setMessage("Delete request ?")
                .setIcon(R.drawable.confirm)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            deleteRequest(getJSONRequestFromCurrentView());
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage(), e);
                            showMessageFail(MainActivity.APP_SEVERE_MSG);
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, null).show();
    }

    private void updateRequest(JSONObject request) throws UnsupportedEncodingException {

        Log.d(TAG, "Update request");
        new AppAsyncHttp(getContext()) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                hideKeyboard();
                showMessageProgress("Loading...");
            }

            @Override
            @SuppressWarnings("deprecation")
            protected void onPostExecute(CustomHttpResponse response) {
                super.onPostExecute(response);
                if (response == null) {
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                    return;
                }
                if (response.code == HttpStatus.SC_OK) {
                    showMessageSuccess("SUCCESS");
                } else if (response.code == HttpStatus.SC_BAD_REQUEST) {
                    Log.d(TAG, "Http call failed: " + response.entity);
                    showMessageFail(response.entity);
                } else {
                    Log.d(TAG, "Http call failed: " + response.entity);
                    showMessageFail(MainActivity.APP_SERVICE_SEVERE_MSG);
                }
            }
        }.updateRequest(request);
    }

    private void deleteRequest(final JSONObject request) throws UnsupportedEncodingException {

        Log.d(TAG, "Delete request");
        new AppAsyncHttp(getContext()) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                hideKeyboard();
                showMessageProgress("Loading...");
            }

            @Override
            @SuppressWarnings("deprecation")
            protected void onPostExecute(CustomHttpResponse response) {
                super.onPostExecute(response);
                if (response == null) {
                    showMessageFail(MainActivity.APP_SEVERE_MSG);
                    return;
                }
                if (response.code == HttpStatus.SC_OK) {
                    ((MainActivity) getActivity()).loadRequestsFragment(getView());
                    try {
                        sharedPrefsResultsManager.removeResult(request.getString("id"));
                        notifyWidgetProviderToUpdateViews(this.context);
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage(), e);
                        showMessageFail(MainActivity.APP_SEVERE_MSG);
                    }
                } else if (response.code == HttpStatus.SC_BAD_REQUEST) {
                    Log.d(TAG, "Http call failed: " + response.entity);
                    showMessageFail(response.entity);
                } else {
                    Log.d(TAG, "Http call failed: " + response.entity);
                    showMessageFail(MainActivity.APP_SERVICE_SEVERE_MSG);
                }
            }
        }.deleteRequest(request);
    }
}
