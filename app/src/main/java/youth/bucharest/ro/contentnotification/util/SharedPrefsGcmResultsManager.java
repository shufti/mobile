package youth.bucharest.ro.contentnotification.util;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Created by fslevoaca on 09.06.16.
 */
public class SharedPrefsGcmResultsManager {

    public static final String GCM_RESULTS_PREFERENCES = "GcmResultsPreferences";

    private SharedPreferences sharedPrefs;

    public SharedPrefsGcmResultsManager(Context context) {
        this.sharedPrefs = context.getSharedPreferences(GCM_RESULTS_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void persistResult(String id, String result) {
        this.sharedPrefs.edit().putString(id, result).commit();
    }

    public String getResult(String id) {
        return this.sharedPrefs.getString(id, null);
    }

    public Map<String, ?> getAllResults() {
        return this.sharedPrefs.getAll();
    }

    public int getResultsListSize() {
        return this.sharedPrefs.getAll().size();
    }

    public void removeResult(String key) {
        this.sharedPrefs.edit().remove(key).commit();
    }

    public void syncResults(List<JSONObject> resultList) throws JSONException {
        this.sharedPrefs.edit().clear().commit();
        for (int i = 0; i < resultList.size(); i++) {
            persistResult(resultList.get(i).getString("id"), resultList.get(i).toString());
        }
    }
}
