package youth.bucharest.ro.contentnotification.services;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import youth.bucharest.ro.contentnotification.R;
import youth.bucharest.ro.contentnotification.util.SharedPrefsGcmResultsManager;
import youth.bucharest.ro.contentnotification.util.UpdateListAdapter;
import youth.bucharest.ro.contentnotification.widgets.ResultsWidgetProvider;

/**
 * Created by raft on 19.05.2016.
 */
public class GcmListenerService extends com.google.android.gms.gcm.GcmListenerService {

    public static final String TAG = "GcmListenerService";

    private SharedPrefsGcmResultsManager sharedPrefsManager;
    private AlarmManager alarmManager;
    NotificationManager notificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Init shared preferences manager from application context");
        this.sharedPrefsManager = new SharedPrefsGcmResultsManager(this);
        Log.d(TAG, "Init alarm manager from application context");
        this.alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        this.notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

    }

    @Override
    public void onMessageReceived(String from, Bundle data) {

        String requestResult = data.getString("result");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + requestResult);

        //Nothing to do here
        if (requestResult == null) {
            return;
        }

        try {
            JSONObject requestResultJson = new JSONObject(requestResult);
            persistResult(requestResultJson);
            if (isMatch(requestResultJson)) {
                sendNotification(requestResultJson);
            }
            notifyWidgetProviderToUpdateViews();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            return;
        }
    }

    private void persistResult(JSONObject result) throws JSONException {
        Log.d(TAG, "Persist result received via GCM " + result);
        this.sharedPrefsManager.persistResult(result.getString("id"), result.toString());
    }

    private void notifyWidgetProviderToUpdateViews() {
        Intent intent = new Intent(ResultsWidgetProvider.ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        this.alarmManager.set(AlarmManager.RTC, 0, pendingIntent);
        Log.d(TAG, "I set a pending intent for widget provider to update the widget views only when CPU is awake");
    }

    private NotificationCompat.Builder getNotificationBuilder() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setCategory(Notification.CATEGORY_SOCIAL)
                        .setSmallIcon(R.drawable.newspaper_small)
                        .setDefaults(Notification.DEFAULT_ALL);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.rss_gray);
        mBuilder.setLargeIcon(bitmap);
        return mBuilder;
    }

    public void sendNotification(JSONObject result) throws JSONException {

        NotificationCompat.Builder builder = getNotificationBuilder();
        builder.setContentTitle(result.getString("label"));
        if (result.getJSONObject("result") == null || result.getJSONObject("result").getString("content") == null || result.getJSONObject("result").getString("content").isEmpty()) {
            builder.setContentText("No result found");
        } else {
            builder.setContentText(result.getJSONObject("result").getString("content"));
        }
        //Onclick notification, open in browser
        builder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, UpdateListAdapter.getOpenInBrowserIntent(result), 0));
        this.notificationManager.notify("shufti", result.getInt("id"), builder.build());

        Log.i(TAG, "Sent notification");
    }

    public boolean isMatch(JSONObject result) throws JSONException {
        return result.getJSONObject("result").getBoolean("match");
    }
}
