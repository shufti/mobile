package youth.bucharest.ro.contentnotification.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import youth.bucharest.ro.contentnotification.R;

/**
 * Created by fslevoaca on 31.05.16.
 */
public class TemplateListAdapter extends BaseAdapter {

    private Context context;
    private JSONArray list;

    public TemplateListAdapter(Context context, JSONArray list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.length();
    }

    @Override
    public Object getItem(int position) {
        try {
            return list.getJSONObject(position);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.template_item, parent, false);
        TextView templateLabel = (TextView) rowView.findViewById(R.id.templateLabelId);

        try {
            JSONObject templateJson = list.getJSONObject(position);
            templateLabel.setText(templateJson.getString("label"));
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return rowView;
    }
}
