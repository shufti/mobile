package youth.bucharest.ro.contentnotification.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by fslevoaca on 02.06.16.
 */
public class SharedPrefsManager {

    public static final String APP_PREFERENCES = "AppPreferences";

    public static final String TOKEN = "TOKEN";
    public static final String IS_TOKEN_REFRESHED = "isTokenRefreshed";

    private SharedPreferences sharedPrefs;

    public SharedPrefsManager(Context context) {
        this.sharedPrefs = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void setToken(String token) {
        this.sharedPrefs.edit().putString(TOKEN, token).commit();
    }

    public String getToken() {
        return this.sharedPrefs.getString(TOKEN, null);
    }

    public void setTokenRefreshed(boolean isTokenRegreshed) {
        this.sharedPrefs.edit().putBoolean(IS_TOKEN_REFRESHED, isTokenRegreshed).commit();
    }

    public boolean isTokenRefreshed() {
        return this.sharedPrefs.getBoolean(IS_TOKEN_REFRESHED, false);
    }
}
