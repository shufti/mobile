package youth.bucharest.ro.contentnotification.util;

/**
 * Created by raft on 19.05.2016.
 */
public class QuickStartPreferences {

    public static final String CLOUD_SYNC_ACTION = "youth.bucharest.ro.contentnotification.CLOUD_SYNC";
    public static final String UPDATE_STATUS = "updateStatus";
}